import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { TotalComponent } from './total/total.component';
import { BranchComponent } from './branch/branch.component';
import { WeekRowComponent } from './week-row/week-row.component';
import { DashbordComponent } from './dashbord/dashbord.component';

@NgModule({
  declarations: [
    AppComponent,
    TotalComponent,
    BranchComponent,
    WeekRowComponent,
    DashbordComponent
  ],
  imports: [
    NgbModule,
    BrowserModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
