import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {Status} from '../model/Status';
@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {
  @Input() total_value:Status[];
  constructor() { }
  ngOnInit(): void {
  }
}
