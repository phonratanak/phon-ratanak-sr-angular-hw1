import { Component, Input, OnInit } from '@angular/core';
import {Week} from '../model/Week';
@Component({
  selector: '[app-branch]',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {
  @Input() week_value:Week;
  constructor() {
   }
  ngOnInit(): void {
  }

}
