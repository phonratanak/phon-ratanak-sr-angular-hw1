import { Component, OnInit,Input } from '@angular/core';
import {Week} from '../model/Week';
@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {
  @Input() week_value:Week;
  @Input() week:number;
  @Input() index:number;
  constructor() { }

  ngOnInit(): void {
  }

}
