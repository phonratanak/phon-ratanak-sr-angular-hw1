
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import {Status} from '../model/Status';
import {Week} from '../model/Week';
@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {

  title = ["BranchA","BranchB"];
  branch_value : Week;
  total_status : Status[];

  ngOnInit(){
    this.total_status = [
      {'used': this.ramdomValue(28), 'available' : 28},
      {'used': this.ramdomValue(29), 'available' : 29},
      {'used': this.ramdomValue(30), 'available' : 30}
    ];
  }

  ramdomValue(used): any{
    return Math.floor((Math.random()*used)+1);
  }

  generationBranch(): any{
    return {
    name : "Week",
    regular : {
      used: this.ramdomValue(28), available : 28
    },
    diesel : {
      used: this.ramdomValue(29), available : 29
    },
    premium : {
      used: this.ramdomValue(30), available : 30
    }
  }
  }
}
